"use strict";

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let html = "<ul>";
const arrItems = arr.map((el) => {
  html += "<li>" + el + "</li>";
  let item = document.createElement("li");
  item.textContent = el;
  return item;
});
html += "</ul>";

console.log(arrItems);
console.log(...arrItems);
document.write(html);
